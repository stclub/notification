<?php
namespace Notification;
class Hide_Cpt {
	public static function init() {
		add_action('wp_dashboard_setup', array(__CLASS__, 'remove_notification' ));
	}
	public static function remove_notification() {
	    if ( current_user_can( 'subscriber' ) ) {
	        remove_meta_box('notification_widget', 'dashboard', 'normal');
	    }
	    if ( current_user_can( 'contributor' ) ) {
	        remove_menu_page( 'edit.php?post_type=notification' );
	    }
	    if ( current_user_can( 'author' ) ) {
	        remove_menu_page( 'edit.php?post_type=notification' );
	    }
	}
}
