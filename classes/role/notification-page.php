<?php
namespace Notification;
class Notification_Page {
	public static function init() {
		add_action('admin_menu', array(__CLASS__, 'add_notification_page'));
	}
	public static function add_notification_page() {
  		add_menu_page('Notifications', 'notifications', 'subscriber', 'notifications', 'notification_data'); 
	}
}
