<?php
namespace Notification;
class Dashboard_widget {
	public static function init() {
        add_action( 'wp_dashboard_setup', array(__CLASS__, 'add_dashboard_widgets' ));
	}
    public static function add_dashboard_widgets() {
        wp_add_dashboard_widget( 'notification_widget', 'Notifications', 'notification_widget' );
    }
}