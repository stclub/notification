<?php
namespace Notification;
class Notification_Register {
	public static function init() {
		add_action('init', array(__CLASS__, 'notification_register'));
	}
	public static function notification_register() {

        $labels = array(
            'name' => _x('Notification', 'post type general name'),
            'singular_name' => _x('Notification ', 'post type singular name'),
            'add_new' => _x('Add New', 'Notification'),
            'add_new_item' => __('Add New Notification'),
            'edit_item' => __('Edit Notification'),
            'new_item' => __('New Notification'),
            'view_item' => __('View Notification'),
            'search_items' => __('Search Notification'),
            'not_found' =>  __('Nothing found'),
            'not_found_in_trash' => __('Nothing found in Trash'),
            'parent_item_colon' => ''
        );

        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'menu_position' => null,
            'supports' => array( 'title', 'editor' )
          ); 

        register_post_type( 'notification' , $args );
    }
}