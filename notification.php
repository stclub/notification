<?php
/**
Plugin Name: notifications
description: notification widget
Version: 1.0
Author: Nadeem
License: MIT
Text Domain: notification
Domain Path: /languages/
*/
    // If this file is accessed directory, then abort.
    defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
    
    require_once(plugin_dir_path(__FILE__) . 'classes/index.php');
    require_once(plugin_dir_path(__FILE__) . 'templates/index.php');
    use Notification\Notification_register; 
    use Notification\Dashboard_widget;  
    use Notification\Notification_Page;  
    use Notification\Hide_Cpt;     

    add_action( 'plugins_loaded', 'notification' );
    function notification() {
        load_plugin_textdomain( 'notification', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
        Notification_register::init();
        Dashboard_widget::init();
        Notification_Page::init();
        Hide_Cpt::init();
    }