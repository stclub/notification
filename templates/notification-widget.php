<?php
    function notification_widget() {
       
       $args = array(
      'post_type'   => 'notification',
      'post_status' => 'publish',
      
    );
     
    $notification = new WP_Query( $args );
    if( $notification->have_posts() ) {

          while( $notification->have_posts() ) {
            $notification->the_post();
            ?>
                <h2><strong>
                    <?php 
                        printf(
                            __( ' %s', 'notification' ),
                            get_the_title()
                        ); 
                   
                    ?>
                </strong></h2>
                <p>
                      <?php 
                        printf(
                            __( ' %s', 'notification' ),
                            get_the_content()
                        ); 
                       
                    ?>  
                </p> 
                 <hr>  
            <?php
          }
          wp_reset_postdata();
        ?>
    <?php
    }else {
      esc_html_e( 'No notification!', 'notification' );
    }
} 