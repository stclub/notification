<?php
    function notification_data() {
        $args = array(
          'post_type'   => 'notification',
          'post_status' => 'publish',
          
         );
     
        $notification = new WP_Query( $args );
        if( $notification->have_posts() ) {
        ?>
          <div style="margin:50px;background-color: #ffffff;padding:30px">
            <h1>Notifications</h1>
            <br><hr>
            <?php
              while( $notification->have_posts() ) {
                $notification->the_post();
                ?>
                    <h3><strong>
                    <?php 
                        printf(
                            __( ' %s', 'notification' ),
                            get_the_title()
                        ); 
                   
                    ?>
                    </strong></h3>
                    <p>
                    <?php 
                        printf(
                            __( ' %s', 'notification' ),
                            get_the_content()
                        ); 
                       
                    ?>  
                    </p> 
                    <hr>  
                <?php
              }
              wp_reset_postdata();
            ?>
            
          </div>
        <?php
        }else {
          esc_html_e( 'No notification!', 'notification' );
        }
    }